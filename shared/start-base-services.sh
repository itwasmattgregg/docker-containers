#! /bin/bash

SCRIPTPATH=$(pwd)

cd "${SCRIPTPATH}/mysql"
docker-compose up -d

cd "${SCRIPTPATH}/postgres"
docker-compose up -d

cd "${SCRIPTPATH}/nginx-proxy"
docker-compose up -d
